VERSION=1.0.1

DST=emmet-mode.el
PYTHON ?= python3

# set markdown input format to "markdown-smart" for pandoc version 2 and to "markdown" for pandoc prior to version 2
MARKDOWN = $(shell if [ `pandoc -v | head -n1 | cut -d" " -f2 | head -c1` = "2" ]; then echo markdown-smart; else echo markdown; fi)

all: emmet-mode.el emmet-mode.elc

emmet-mode.el: src/snippets.el src/preferences.el src/*
	rm -f $(DST)
	touch $(DST)
	cat src/comments.el >> $(DST)
	cat src/init.el >> $(DST)
	cat src/mode-def.el >> $(DST)
	cat src/snippets.el >> $(DST)
	cat src/preferences.el >> $(DST)
	cat src/html-abbrev.el >> $(DST)
	cat src/lorem.el >> $(DST)
	cat src/css-abbrev.el >> $(DST)
	echo "" >> $(DST)
	echo ";;; emmet-mode.el ends here" >> $(DST)

emmet-mode.elc: emmet-mode.el
	emacs --batch --eval '(byte-compile-file "emmet-mode.el")'

src/snippets.el: conf/snippets.json
	$(PYTHON) tools/json2hash conf/snippets.json -o src/snippets.el --defvar 'emmet-snippets'

src/preferences.el: conf/preferences.json
	$(PYTHON) tools/json2hash conf/preferences.json -o src/preferences.el --defvar 'emmet-preferences'

clean:
	rm -f emmet-mode.elc emmet-mode.el README.txt src/snippets.el src/preferences.el emmet-mode-$(VERSION).tar.gz

test: emmet-mode.el
	emacs --quick --script src/test.el

README.txt: README.md
	pandoc -f $(MARKDOWN) -t plain README.md -o README.txt

docs: README.txt
	@echo generated docs

dist: docs
	@tar -czf emmet-mode-$(VERSION).tar.gz --transform "s|^|emmet-mode-$(VERSION)/|" --owner 0 --group 0 \
		--exclude '*.DS_Store' \
		--exclude '*.kate-swp' \
		--exclude '.gitlab-ci.yml' \
		--exclude '*.elc' \
		--exclude '*~' \
		--exclude '.git' \
		-- \
		conf src tools LICENSE Makefile \
		README.md README.txt AUTHORS

.PHONY: all test docs clean
